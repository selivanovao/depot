#---
# Excerpted from "Agile Web Development with Rails",
# published by The Pragmatic Bookshelf.
# Copyrights apply to this code. It may not be used to create training material, 
# courses, books, articles, and the like. Contact us if you are in doubt.
# We make no guarantees that this code is fit for any purpose. 
# Visit http://www.pragmaticprogrammer.com/titles/rails4 for more book information.
#---
# encoding: utf-8
Product.delete_all
Product.create!(title: 'Product one',
  description: 
    %{
        description one
      },
  image_url:   'cs.jpg',    
  price: 36.00)

Product.create!(title: 'Product two',
  description:
    %{ description two },
  image_url: 'ruby.jpg',
  price: 49.95)

Product.create!(title: 'Product three ',
  description: 
    %{description three },
  image_url: 'rtp.jpg',
  price: 34.95)
